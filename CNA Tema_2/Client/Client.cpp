#include <iostream>

#include "../Generated/PrintStarSign.grpc.pb.h"

#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;

void processDate(std::string date, int& day, int& month, int& year)
{
	int index = 0;

	while (date[index] != '/')
	{
		month = month * 10 + (date[index] - '0');
		index++;
	}

	index++;

	while (date[index] != '/')
	{
		day = day * 10 + (date[index] - '0');
		index++;
	}

	index++;

	while (index < date.size())
	{
		year = year * 10 + (date[index] - '0');
		index++;
	}
}

void printError(int errorCode)
{
	system("CLS");
	
	switch (errorCode)
	{
	case 1:
		std::cout << "\n Unknown symbols detected. Please use only numbers and '/'!";
		break;

	case 2:
		std::cout << "\n Invalid day value. Day must be between 1 and 31 (29 for february)!";
		break;

	case 3:
		std::cout << "\n Invalid month value. Month must be between 1 and 12!";
		break;

	case 4:
		std::cout << "\n Invalid year value. Year must be at least 1!";
		break;

	case 5:
		std::cout << "\n The date does not exist!";
		break;
	}
}

bool isLeapYear(int year)
{
	if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
		return 1;
	return 0;
}

bool isValidDate(std::string& date)
{
	int day = 0, month = 0, year = 0;

	for (int index = 0; index < date.length(); index++)
	{
		if ((date[index] < '0' || date[index] > '9') && date[index] != '/')
		{
			printError(1); // invalid symbols
			return 0;
		}
	}

	processDate(date, day, month, year);

	if ((day <= 0 || day > 31) || (month == 2 && day > 29))
	{
		printError(2);// invalid value for day
		return 0;
	}

	if (month <= 0 || month > 12)
	{
		printError(3);// invalid value for month
		return 0;
	}

	if (year <= 0)
	{
		printError(4);// invalid value for year
		return 0;
	}

	if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31)
	{
		printError(5);// non-existing date
		return 0;
	}

	if (month == 2)
	{
		if (!isLeapYear(year) && day == 29)
		{
			printError(5);// non-existing date
			return 0;
		}
	}

	if (month < 10 && date[0] != '0')
		date.insert(0, "0");

	if (day < 10 && date[3] != '0')
		date.insert(3, "0");

	return 1;
}

int main()
{
	grpc_init();

	ClientContext context;
	auto print_stub = PrintStarSignService::NewStub(grpc::CreateChannel("localhost:8888", grpc::InsecureChannelCredentials()));

	std::string date;

	std::cout << "\n\n\n Please enter a valid date (m/d/y): ";

	bool OK = 0;

	while (OK == 0)
	{
		std::cin >> date;

		if (isValidDate(date))
		{
			OK = 1;
		}
		else
		{
			std::cout << "\n\n Please enter a valid date (m/d/y): ";
		}
	}

	PrintRequest printRequest;
	printRequest.set_date(date);
	PrintResponse printResponse;
	auto status = print_stub->Print(&context, printRequest, &printResponse);
	std::cout << "\n Congratulations, you are " << printResponse.result() << "!\n\n";
}
