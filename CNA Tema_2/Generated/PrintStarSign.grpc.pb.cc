// Generated by the gRPC C++ plugin.
// If you make any local change, they will be lost.
// source: PrintStarSign.proto

#include "PrintStarSign.pb.h"
#include "PrintStarSign.grpc.pb.h"

#include <functional>
#include <grpcpp/impl/codegen/async_stream.h>
#include <grpcpp/impl/codegen/async_unary_call.h>
#include <grpcpp/impl/codegen/channel_interface.h>
#include <grpcpp/impl/codegen/client_unary_call.h>
#include <grpcpp/impl/codegen/client_callback.h>
#include <grpcpp/impl/codegen/message_allocator.h>
#include <grpcpp/impl/codegen/method_handler.h>
#include <grpcpp/impl/codegen/rpc_service_method.h>
#include <grpcpp/impl/codegen/server_callback.h>
#include <grpcpp/impl/codegen/server_callback_handlers.h>
#include <grpcpp/impl/codegen/server_context.h>
#include <grpcpp/impl/codegen/service_type.h>
#include <grpcpp/impl/codegen/sync_stream.h>

static const char* PrintStarSignService_method_names[] = {
  "/PrintStarSignService/Print",
};

std::unique_ptr< PrintStarSignService::Stub> PrintStarSignService::NewStub(const std::shared_ptr< ::grpc::ChannelInterface>& channel, const ::grpc::StubOptions& options) {
  (void)options;
  std::unique_ptr< PrintStarSignService::Stub> stub(new PrintStarSignService::Stub(channel));
  return stub;
}

PrintStarSignService::Stub::Stub(const std::shared_ptr< ::grpc::ChannelInterface>& channel)
  : channel_(channel), rpcmethod_Print_(PrintStarSignService_method_names[0], ::grpc::internal::RpcMethod::NORMAL_RPC, channel)
  {}

::grpc::Status PrintStarSignService::Stub::Print(::grpc::ClientContext* context, const ::PrintRequest& request, ::PrintResponse* response) {
  return ::grpc::internal::BlockingUnaryCall(channel_.get(), rpcmethod_Print_, context, request, response);
}

void PrintStarSignService::Stub::experimental_async::Print(::grpc::ClientContext* context, const ::PrintRequest* request, ::PrintResponse* response, std::function<void(::grpc::Status)> f) {
  ::grpc_impl::internal::CallbackUnaryCall(stub_->channel_.get(), stub_->rpcmethod_Print_, context, request, response, std::move(f));
}

void PrintStarSignService::Stub::experimental_async::Print(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::PrintResponse* response, std::function<void(::grpc::Status)> f) {
  ::grpc_impl::internal::CallbackUnaryCall(stub_->channel_.get(), stub_->rpcmethod_Print_, context, request, response, std::move(f));
}

void PrintStarSignService::Stub::experimental_async::Print(::grpc::ClientContext* context, const ::PrintRequest* request, ::PrintResponse* response, ::grpc::experimental::ClientUnaryReactor* reactor) {
  ::grpc_impl::internal::ClientCallbackUnaryFactory::Create(stub_->channel_.get(), stub_->rpcmethod_Print_, context, request, response, reactor);
}

void PrintStarSignService::Stub::experimental_async::Print(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::PrintResponse* response, ::grpc::experimental::ClientUnaryReactor* reactor) {
  ::grpc_impl::internal::ClientCallbackUnaryFactory::Create(stub_->channel_.get(), stub_->rpcmethod_Print_, context, request, response, reactor);
}

::grpc::ClientAsyncResponseReader< ::PrintResponse>* PrintStarSignService::Stub::AsyncPrintRaw(::grpc::ClientContext* context, const ::PrintRequest& request, ::grpc::CompletionQueue* cq) {
  return ::grpc_impl::internal::ClientAsyncResponseReaderFactory< ::PrintResponse>::Create(channel_.get(), cq, rpcmethod_Print_, context, request, true);
}

::grpc::ClientAsyncResponseReader< ::PrintResponse>* PrintStarSignService::Stub::PrepareAsyncPrintRaw(::grpc::ClientContext* context, const ::PrintRequest& request, ::grpc::CompletionQueue* cq) {
  return ::grpc_impl::internal::ClientAsyncResponseReaderFactory< ::PrintResponse>::Create(channel_.get(), cq, rpcmethod_Print_, context, request, false);
}

PrintStarSignService::Service::Service() {
  AddMethod(new ::grpc::internal::RpcServiceMethod(
      PrintStarSignService_method_names[0],
      ::grpc::internal::RpcMethod::NORMAL_RPC,
      new ::grpc::internal::RpcMethodHandler< PrintStarSignService::Service, ::PrintRequest, ::PrintResponse>(
          std::mem_fn(&PrintStarSignService::Service::Print), this)));
}

PrintStarSignService::Service::~Service() {
}

::grpc::Status PrintStarSignService::Service::Print(::grpc::ServerContext* context, const ::PrintRequest* request, ::PrintResponse* response) {
  (void) context;
  (void) request;
  (void) response;
  return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
}


