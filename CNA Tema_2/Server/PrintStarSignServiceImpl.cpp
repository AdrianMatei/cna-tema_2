#include "PrintStarSignServiceImpl.h"

PrintStarSignServiceImpl::PrintStarSignServiceImpl(std::vector<std::pair<std::string, std::string>> intervals, std::vector<std::string> starSigns)
{
	this->intervals = intervals;
	this->starSigns = starSigns;
}

::grpc::Status PrintStarSignServiceImpl::Print(::grpc::ServerContext* context, const::PrintRequest* request, ::PrintResponse* response)
{
	std::string date = request->date();
	std::string starSign;

	int index = date.length() - 1;

	while (date[index] != '/')
	{
		date.erase(date.begin() + index);
		index--;
	}
	date.erase(date.begin() + index);

	for (int index = 0; index < intervals.size(); index++)
	{
		if (date >= intervals[index].first && date <= intervals[index].second)
		{
			response->set_result(starSigns[index]);
			//std::cout << "\n Congratulations, you are " << starSigns[index] << "!\n";
		}
	}

	return ::grpc::Status::OK;
}