#pragma once

#include "..\Generated\PrintStarSign.grpc.pb.h"

class PrintStarSignServiceImpl final : public PrintStarSignService::Service
{
private:
	std::vector<std::pair<std::string, std::string>> intervals;
	std::vector<std::string> starSigns;

public:
	PrintStarSignServiceImpl(std::vector<std::pair<std::string, std::string>> intervals, std::vector<std::string> starSigns);
	::grpc::Status Print(::grpc::ServerContext* context, const ::PrintRequest* request, ::PrintResponse* response) override;
};

