#include <iostream>
#include <fstream>

#include "PrintStarSignServiceImpl.h"

#include <grpc/grpc.h>
#include <grpcpp/server.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/server_context.h>

int main()
{
	std::string server_address("localhost:8888");

	std::vector<std::pair<std::string, std::string>> intervals;
	std::vector<std::string> starSigns;

	std::ifstream fin("StarSigns.txt");

	while (!fin.eof())
	{
		std::pair<std::string, std::string> interval;
		std::string starSign;

		fin >> interval.first >> interval.second >> starSign;
		intervals.push_back(interval);
		starSigns.push_back(starSign);
	}

	PrintStarSignServiceImpl service(intervals, starSigns);

	::grpc_impl::ServerBuilder serverBuilder;

	serverBuilder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
	serverBuilder.RegisterService(&service);

	std::unique_ptr<::grpc_impl::Server> server(serverBuilder.BuildAndStart());
	std::cout << "Server listening on " << server_address << std::endl << std::endl;

	server->Wait();
}
